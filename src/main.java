public class main {
    public static void main(String[] args) {
        canComplete("butl", "beautiful");
        canComplete("butlz", "beautiful");
        canComplete("tulb", "beautiful");
        canComplete("buu", "beautiful");
        canComplete("12345", "beautiful");
        canComplete("ual","beautiful");
    }

    public static void canComplete(String input, String output) {
        boolean canBeCompleted = false;
        int charFoundAtIndex = 0;
        input = input.toLowerCase();
        char[] inputArray = input.toCharArray();
        output = output.toLowerCase();
        char[] outputArray = output.toCharArray();

        inputloop:
        for (int i = 0; i < inputArray.length; i++){
            for(int j = charFoundAtIndex; j < outputArray.length; j++){
                if(inputArray[i] == outputArray[j]){
                    charFoundAtIndex = j + 1;
                    if(i == inputArray.length-1)canBeCompleted = true;
                    break;
                }
                else if(j == outputArray.length -1) {
                    canBeCompleted = false;
                    break inputloop;
                }
            }
        }
        if (canBeCompleted) System.out.println(input + " " + output + "\n" + "String can be completed");
        else System.out.println(input + " " + output + "\n" + "String can not be completed");

    }
}